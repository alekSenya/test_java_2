import javax.swing.*;
import javax.swing.text.AbstractDocument;

import java.awt.*;

public class Main {
    public static void main(String[] args) {
        //Создаем форму с именем "Test java #2"
        JFrame frame = new JFrame("Test java #2");
        //Добавляем в фрейм, что делать после закрытия окна программы
        //(EXIT_ON_CLOSE - константа, которая говорит о том, что программа завершит работу после закрытия окна)
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        //Устанавливаем размеры окна
        frame.setSize(500, 200);

        //Создаем текстовое поле в которое в дальнейшем будем добавлять исходные данные
        JTextField tFieldSource = new JTextField();
        //Устанавливаем размер этого поля
        tFieldSource.setPreferredSize(new Dimension(100, 25));
        //Добавляем фильтр для того, чтобы в данное поле можно было вписать только целые числа
        ((AbstractDocument) tFieldSource.getDocument()).setDocumentFilter(new MyDocumentFilter());

        //Создаем текстовое поле в которое в дальнейшем будем выводить полученный результат
        JTextField tFieldResult = new JTextField();
        //Устанавливаем размер этого поля
        tFieldResult.setPreferredSize(new Dimension(100, 25));
        //Делаем поле неизменяемым для пользователя
        tFieldResult.setEditable(false);

        //Создаем выпадающий список добавляя в него созданное нами перечисление (Units)
        JComboBox cBoxFrom = new JComboBox(Units.values());
        //Устанавливаем размер этого выпадающего списока
        cBoxFrom.setSize(100, 50);

        //Создаем выпадающий список добавляя в него созданное нами перечисление (Units)
        JComboBox cBoxTo = new JComboBox(Units.values());
        //Устанавливаем размер этого выпадающего списока
        cBoxTo.setSize(100, 50);

        //Создаем наш ActionListener/DocumentListener в конструктор которого передаем все элементы формы с которыми будем взаимоействовать
        MyActionListener listener = new MyActionListener(tFieldSource, tFieldResult, cBoxFrom, cBoxTo);
        //Добавляем к выпадающим спискам наш ActionListener
        cBoxFrom.addActionListener(listener);
        cBoxTo.addActionListener(listener);
        //Добавляем к полю с исходными данными наш DocumentListener
        //(В классе MyActionListener у нас реализованы сразу 2 интерфейса ActionListener и DocumentListener
        // поэтому мы можем использовать объект этого класса и при передаче ActionListener'a и DocumentListener'a)
        tFieldSource.getDocument().addDocumentListener(listener);

        //Для того, чтобы в дальнейшем добавлять на форму другие элементы, получаем объект Container нашего фрейма
        Container content = frame.getContentPane();
        //Устанавливаем layout (говорит о том, как будут распологаться элементы на форме. FlowLayout говорит о том,
        // что элеметы будут распологаться в одну строку пока им хватает в ней места, если места не хватает перемещает
        // элемент на след. строку) для нашей формы
        content.setLayout(new FlowLayout());
        //Добавляем текстовое поле, созданное нами ранее, с исходными данными на форму
        content.add(tFieldSource);
        //Добавляем выпадающий список, созданное нами ранее, "из чего" будем переводить на форму
        content.add(cBoxFrom);
        //Добавляем выпадающий список, созданное нами ранее, "во что" будем переводить на форму
        content.add(cBoxTo);
        //Добавляем текстовое поле, созданное нами ранее, в котором будем отображать результат на форму
        content.add(tFieldResult);

        //Устанавливаем, чтобы форма открывалась по центру экрана
        frame.setLocationRelativeTo(null);

        //Делаем форму видимой
        frame.setVisible(true);
    }
}