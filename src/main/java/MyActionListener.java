import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Класс реализующий интерфейсы ActionListener, DocumentListener
 *
 * ActionListener - необходим нам, для выпадающих списков, чтобы отслеживать изменение выбора
 * DocumentListener - необходим нам, для текстового поля в которое вводится начальное значение,
 * чтобы отслеживать изменение значения в нем
 */
public class MyActionListener implements ActionListener, DocumentListener {
    private final JTextField tFieldSource;
    private final JTextField tFieldResult;
    private final JComboBox cBoxFrom;
    private final JComboBox cBoxTo;

    /**
     * Конструтор нашего класса
     *
     * @param tFieldSource - текстовое поле в которое вводится начальное значение
     * @param tFieldResult - текстовое поле в которое выводится результат вычислений
     * @param cBoxFrom - выпадающий список "из чего" будем переводить
     * @param cBoxTo - выпадающий список "во что" будем переводить
     */
    MyActionListener(JTextField tFieldSource, JTextField tFieldResult, JComboBox cBoxFrom, JComboBox cBoxTo) {
        this.tFieldSource = tFieldSource;
        this.tFieldResult = tFieldResult;
        this.cBoxFrom = cBoxFrom;
        this.cBoxTo = cBoxTo;
    }

    public void actionPerformed(ActionEvent e) {
        calculation();
    }

    public void insertUpdate(DocumentEvent e) {
        calculation();
    }

    public void removeUpdate(DocumentEvent e) {
        calculation();
    }

    public void changedUpdate(DocumentEvent e) {
        calculation();
    }

    /**
     * Метод производящий расчет и добавляющий полученное значение на tFieldResult
     */
    private void calculation() {
        //Если в tFieldSource содержится текст длинной > 0
        if (tFieldSource.getText().length() > 0) {
            //Получаем то, "из чего" переводить
            Units selectedItemFrom = (Units) cBoxFrom.getSelectedItem();
            //Получаем то, "во что" переводить
            Units selectedItemTo = (Units) cBoxTo.getSelectedItem();
            //Получаем исходное значение
            String src = tFieldSource.getText();
            //Строку полученную на предыдущем шаге, переводим в BigDecimal (Переводим в BigDecimal специально,
            //чтобы диапозон значений, которые может расчитать наш "калькулятор" был максимально большим)
            BigDecimal bdFieldSource = BigDecimal.valueOf(Integer.parseInt(src));
            //получаем результат вычислений и переводим его в строку.
            //(исходное значение * кол-во миллисекунд (зависит от выбранного элементы "из чего") /
            //кол-во миллисекунд (зависит от выбранного элементы "во что"))
            String result = String.valueOf(
                    bdFieldSource
                            .multiply(selectedItemFrom.getMilliseconds())
                            //6 - кол-во знаков после запятой, RoundingMode.HALF_UP - округление в большую сторону
                            .divide(selectedItemTo.getMilliseconds(), 6, RoundingMode.HALF_UP)
            );
            //Записываем результат в поле tFieldResult
            tFieldResult.setText(result);
        //Иначе
        } else {
            //Пишем в поле tFieldResult "0"
            tFieldResult.setText("0");
        }
    }
}
