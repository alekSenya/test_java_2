import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;
import java.awt.*;

/**
 * Класс расширяющий DocumentFilter
 */
public class MyDocumentFilter extends DocumentFilter {

    @Override
    public void replace(FilterBypass fb, int offset, int length, String text,
                        AttributeSet attrs) throws BadLocationException {
        Document doc = fb.getDocument();
        StringBuilder sb = new StringBuilder();
        sb.append(doc.getText(0, doc.getLength()));
        sb.replace(offset, offset + length, text);

        //Проверяем является ли введеный символ числом или путой строкой
        if (isDigitalOrIsEmpty(sb.toString())) {
            //если да, то пишем его в поле
            super.replace(fb, offset, length, text, attrs);
        } else {
            //если нет, сигнализируем об этом
            Toolkit.getDefaultToolkit().beep();
        }
    }

    @Override
    public void remove(FilterBypass fb, int offset, int length)
            throws BadLocationException {
        Document doc = fb.getDocument();
        StringBuilder sb = new StringBuilder();
        sb.append(doc.getText(0, doc.getLength()));
        sb.delete(offset, offset + length);

        //Проверяем является ли введеный символ числом или путой строкой
        if (isDigitalOrIsEmpty(sb.toString())) {
            //если да, то пишем его в поле
            super.remove(fb, offset, length);
        } else {
            //если нет, сигнализируем об этом
            Toolkit.getDefaultToolkit().beep();
        }
    }

    /**
     * Метод определяющий является ли переданная в него строка числом или пустой
     *
     * @param text - строка которую необходимо проверить
     * @return true - строка явл. числом или пустой / false - иначе
     */
    private boolean isDigitalOrIsEmpty(String text) {
        try {
            Integer.parseInt(text);
            return true;
        } catch (NumberFormatException e) {
            return text.equals("");
        }
    }
}
