import java.math.BigDecimal;

/**
 * Перечисление для выпадающих списков
 */
public enum Units {
    WEEK("Недели", 604800000),
    DAY("Сутки", 86400000),
    HOUR("Часы", 3600000),
    MINUTE("Минуты", 60000),
    SECOND("Секунды", 1000),
    MILLISECOND("Миллесекунды", 1);

    //То что будет выводиться на экран ("Недели"/"Сутки"/...)
    private final String display;
    //Кол-во миллисекунд в каждом из элементов (604800000/86400000/...)
    private final int milliseconds;

    /**
     * Конструктор перечисления
     *
     * @param text то, что будет выводиться на экран
     * @param milliseconds кол-во миллисекунд
     */
    Units(String text, int milliseconds) {
        this.display = text;
        this.milliseconds = milliseconds;
    }

    /**
     * Метод для получения миллисекунд в элементе
     *
     * @return кол-во миллисекунд
     */
    public BigDecimal getMilliseconds() {
        return new BigDecimal(milliseconds);
    }

    /**
     * Переопределяем метод toString(), чтобы отображалось то, что нам необходимо
     *
     * @return текст, который будет выводиться на экран
     */
    @Override
    public String toString() {
        return display;
    }
}
